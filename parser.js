'use strict';

var jsdom = require('jsdom');
var fs = require('fs');
var jquery = fs.readFileSync('./jquery.js', 'utf-8');
var http = require('http');

/**
 * @param file: filename
 * #param selector: jQuery css selector
 * @param selectorCallback: callback function that takees
 *        the slected jQuery element as first parameter
 */
function getInformation(file, selector, selectorCallback) {
  jsdom.env({
    file: file,
    src: [jquery],
    done: function (errors, window) {
      var $ = window.$;
      $(selector).each(function() {
        selectorCallback($(this));
      });
    }
  });
}

/**
downloads files from the internet and saves them.
 */



/**
 * @param jqContent jQuery selected element
 */
function selectorCallbackText(jqContent) {
  console.log('text: ', jqContent.text()); // select the text
}

// just for test purpose
function selectorCallbackHref(jqContent) {
  console.log('href: ', jqContent.attr('href')); // select the href
}


/**
 * Start script
 */




// Fangrate
// $$('table.right.round.innerround > tbody > tr:nth-child(19) > td')
getInformation(
  '1.min.html',
  'table.right.round.innerround > tbody > tr:nth-child(19) > td',
  selectorCallbackText
);

// Größe
getInformation(
  '1.min.html',
  'table.right.round.innerround > tbody > tr:nth-child(20) > td',
  selectorCallbackText
);


// Allgemeine Informationen => Fähigkeiten
getInformation(
  '1.min.html',
  'table.right.round.innerround > tbody > tr:nth-child(17) > td:nth-child(2) > a',
  function(jqContent) {
    selectorCallbackHref(jqContent);
    selectorCallbackText(jqContent);
  }
);

// sounds
// $$('table.right.round.innerround > tbody > tr:nth-child(33) > td:nth-child(2) > audio > source')

getInformation(
  '1.min.html',
  'table.right.round.innerround > tbody > tr:nth-child(33) > td:nth-child(2) > audio > source',
  function selectorCallbackSrc(jqContent) {
    var src = jqContent.attr('src');

    var pieces = src.split('/');
    var file = fs.createWriteStream(pieces[pieces.length-1]);
    var request = http.get(src, function(response) {
      response.pipe(file);
    });
    console.log('src: ', src); // select the href
    // TODO get file and save it
  }
);

